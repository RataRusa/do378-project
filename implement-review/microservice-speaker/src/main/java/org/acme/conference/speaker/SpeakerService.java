package org.acme.conference.speaker;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.transaction.Transactional;


@ApplicationScoped
public class SpeakerService {

    public List<Speaker> listAll(){
        return Speaker.listAll();
    }

    public Optional<Speaker> findByUuid(UUID uuid){
        return Speaker.findByUuid(uuid);
    }

    public List<Speaker> search(String query){
        return Speaker.search(query);
    }

    @Transactional  
    public Speaker create(Speaker speaker) {
        speaker.persist();
        return speaker;
    }
    
    @Transactional  
    public void deleteByUuid(UUID uuid) {
        Speaker.deleteByUuid(uuid);
    }
    
}
